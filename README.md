The basics of building a Blockchain in Python

A nice blog by Dante Sblendorio is available to walk through the logic behind the code, at <A HREF="https://www.activestate.com/blog/how-to-build-a-blockchain-in-python/">https://www.activestate.com/blog/how-to-build-a-blockchain-in-python/</A>


changes since forking
* clean-up to comply with Python style
* vary output format by Content-type header (json or html)
* add template to beautify html output
* add sample transactions to help illustration
